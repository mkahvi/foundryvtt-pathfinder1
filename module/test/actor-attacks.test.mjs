import { createTestActor, addCompendiumItemToActor, unitTest_renderActorSheet } from "./actor-utils.mjs";

/**
 * Actor attack tests
 */
export const registerActorItemAttackTests = () => {
  // ---------------------------------- //
  // Actor stats                        //
  // ---------------------------------- //
  quench.registerBatch(
    "pf1.actor.items.attacks",
    async (context) => {
      const { describe, it, expect, before, after } = context;

      /**
       * Handles a shared context to pass between functions
       *
       * @type {object}
       */
      const shared = {};
      /** @type {ActorPF} */
      let actor;
      const messages = [];

      before(async () => {
        actor = await createTestActor({ system: { abilities: { str: { value: 18 } } } });
        shared.actor = actor;
      });

      after(async () => {
        await actor.delete();
      });

      after(async () => {
        // Ensure deletion happens for valid targets no matter if the tests return something else
        const msgIds = messages.filter((msg) => msg instanceof ChatMessage).map((msg) => msg.id);

        await ChatMessage.implementation.deleteDocuments(msgIds);
      });

      describe("longsword", function () {
        const items = {};
        before(async () => {
          items.wLongsword = await addCompendiumItemToActor(actor, "pf1.weapons-and-ammo", "Longsword");
          items.aLongsword = await Item.implementation.create(
            pf1.documents.item.ItemAttackPF.fromItem(items.wLongsword),
            { parent: actor }
          );
        });

        it("add longsword", function () {
          expect(actor.itemTypes.weapon).to.be.an("array").with.lengthOf(1);
          expect(actor.itemTypes.weapon.find((o) => o === items.wLongsword).name).to.equal("Longsword");
          expect(actor.itemTypes.attack).to.be.an("array").with.lengthOf(1);
          expect(actor.itemTypes.attack.find((o) => o === items.aLongsword).name).to.equal("Longsword");
        });

        describe("attack with weapon", function () {
          let msg;
          let rolls;
          before(async () => {
            const usage = await items.wLongsword.use({ skipDialog: true });
            msg = usage.shared.message;
            messages.push(msg);
            rolls = msg.systemRolls.attacks[0];
          });

          it("should have the correct attack formula", function () {
            const parts = rolls.attack.formula.split(" + ");
            expect(parts.shift()).to.equal("1d20");
            const expected = new Set(["4[Strength]"]);
            expect(new Set(parts)).to.deep.equal(expected);
          });

          it("should have the correct damage formula", function () {
            const parts = rolls.damage[0].formula.split(" + ");
            const expected = new Set(["4[Strength]", "sizeRoll(1, 8, 4)"]);
            expect(new Set(parts)).to.deep.equal(expected);
          });

          it("should be a ChatMessage", function () {
            expect(msg).to.be.instanceOf(pf1.documents.ChatMessagePF);
          });
        });

        describe("attack with attack", function () {
          let msg;
          let rolls;
          before(async () => {
            const usage = await items.aLongsword.use({ skipDialog: true });
            msg = usage.shared.message;
            messages.push(msg);
            rolls = msg.systemRolls.attacks[0];
          });

          it("should have the correct attack formula", function () {
            const parts = rolls.attack.formula.split(" + ");
            expect(parts.shift()).to.equal("1d20");
            const expected = new Set(["4[Strength]", "sizeRoll(1, 8, 4)"]);
            expect(new Set(parts)).to.deep.equal(expected);
          });

          it("should have the correct damage formula", function () {
            const parts = rolls.damage[0].formula.split(" + ");
            const expected = new Set(["4[Strength]", "sizeRoll(1, 8, 4)"]);
            expect(new Set(parts)).to.deep.equal(expected);
          });

          it("should be a ChatMessage", function () {
            expect(msg).to.be.instanceOf(pf1.documents.ChatMessagePF);
          });

          describe("size changes", function () {
            describe("tiny size", function () {
              let msg;
              let rolls;
              let prevSize;
              before(async () => {
                prevSize = actor.system.traits.size.base;
                await actor.update({ "system.traits.size": "tiny" });
                const usage = await items.aLongsword.use({ skipDialog: true });
                msg = usage.shared.message;
                messages.push(msg);
                rolls = msg.systemRolls.attacks[0];
              });
              after(async () => {
                await actor.update({ "system.traits.size": prevSize });
              });

              it("should have the correct attack formula", function () {
                const parts = rolls.attack.formula.split(" + ");
                expect(parts.shift()).to.equal("1d20");
                const expected = new Set(["2[Size]", "4[Strength]"]);
                expect(new Set(parts)).to.deep.equal(expected);
              });

              it("should have the correct damage formula", function () {
                const parts = rolls.damage[0].formula.split(" + ");
                const expected = new Set(["4[Strength]", "sizeRoll(1, 8, 2)"]);
                expect(new Set(parts)).to.deep.equal(expected);
              });
            });

            describe("huge size", function () {
              let msg;
              let rolls;
              let prevSize;
              before(async () => {
                prevSize = actor.system.traits.size;
                await actor.update({ "system.traits.size": "huge" });
                const usage = await items.aLongsword.use({ skipDialog: true });
                msg = usage.shared.message;
                messages.push(msg);
                rolls = msg.systemRolls.attacks[0];
              });
              after(async () => {
                await actor.update({ "system.traits.size": prevSize });
              });

              it("should have the correct attack formula", function () {
                const parts = rolls.attack.formula.split(" + ");
                expect(parts.shift()).to.equal("1d20");
                const expected = new Set(["-2[Size]", "4[Strength]"]);
                expect(new Set(parts)).to.deep.equal(expected);
              });

              it("should have the correct damage formula", function () {
                const parts = rolls.damage[0].formula.split(" + ");
                const expected = new Set(["4[Strength]", "sizeRoll(1, 8, 6)"]);
                expect(new Set(parts)).to.deep.equal(expected);
              });
            });
          });
        });
      });

      // reach weapon
      describe("reach weapon range", function () {
        const items = {};
        before(async () => {
          items.wGuisarme = await addCompendiumItemToActor(actor, "pf1.weapons-and-ammo", "Guisarme");
        });

        it("add guisarme", function () {
          expect(actor.itemTypes.weapon.find((o) => o === items.wGuisarme).name).to.equal("Guisarme");
        });

        it("should have max range of 10 ft", function () {
          const maxRange = items.wGuisarme.defaultAction.maxRange;
          expect(pf1.utils.convertDistanceBack(maxRange)[0]).to.equal(10);
        });

        it("should have min range of 5 ft", function () {
          const minRange = items.wGuisarme.defaultAction.minRange;
          expect(pf1.utils.convertDistanceBack(minRange)[0]).to.equal(5);
        });
      });

      describe("attack with natural attack", function () {
        const items = {};
        before(async () => {
          items.bite = await Item.implementation.create(
            {
              type: "attack",
              name: "Bite",
              system: {
                subType: "natural",
                primaryAttack: true,
                actions: [
                  new pf1.components.ItemAction({
                    name: "Bite",
                    actionType: "mwak",
                    damage: {
                      parts: [
                        {
                          formula: "sizeRoll(1, 6, @size)",
                          // Following is outdated but works as test of model migrateData
                          type: { custom: "", values: ["bludgeoning", "piercing", "slashing"] },
                        },
                      ],
                    },
                    ability: {
                      attack: "str",
                      damage: "str",
                      damageMult: 1.5,
                    },
                  }).toObject(),
                ],
              },
            },
            { parent: actor }
          );
        });

        describe("attack with bite", function () {
          let msg;
          let rolls;
          before(async () => {
            const usage = await items.bite.use({ skipDialog: true });
            msg = usage.shared.message;
            messages.push(msg);
            rolls = msg.systemRolls.attacks[0];
          });

          it("should have the correct attack formula", function () {
            const parts = rolls.attack.formula.split(" + ");
            expect(parts.shift()).to.equal("1d20");
            const expected = new Set(["4[Strength]"]);
            expect(new Set(parts)).to.deep.equal(expected);
          });

          it("should have the correct damage formula", function () {
            const parts = rolls.damage[0].formula.split(" + ");
            const expected = new Set(["6[Strength]", "sizeRoll(1, 6, 4)"]);
            expect(new Set(parts)).to.deep.equal(expected);
          });

          it("should be a ChatMessage", function () {
            expect(msg).to.be.instanceOf(pf1.documents.ChatMessagePF);
          });

          describe("as secondary attack", function () {
            let msg;
            let rolls;
            before(async () => {
              const action = items.bite.defaultAction;
              await action.update({ naturalAttack: { primaryAttack: false } });
              const usage = await items.bite.use({ skipDialog: true });
              msg = usage.shared.message;
              messages.push(msg);
              rolls = msg.systemRolls.attacks[0];
            });

            it("should have the correct attack formula", function () {
              const parts = rolls.attack.formula.split(" + ");
              expect(parts.shift()).to.equal("1d20");
              const expected = new Set(["-5[Secondary Attack]", "4[Strength]"]);
              expect(new Set(parts)).to.deep.equal(expected);
            });

            it("should have the correct damage formula", function () {
              const parts = rolls.damage[0].formula.split(" + ");
              const expected = new Set(["2[Strength]", "sizeRoll(1, 6, 4)"]);
              expect(new Set(parts)).to.deep.equal(expected);
            });
          });
        });

        // ---------------------------------- //
        // Render sheet                       //
        // ---------------------------------- //
        unitTest_renderActorSheet(shared, context);
      });

      describe("composite longbow", function () {
        const items = {};
        let originalError;
        const errorNotifications = [];
        before(async () => {
          originalError = ui.notifications.error;
          ui.notifications.error = (message, options) => {
            errorNotifications.push(message);
            return originalError.call(ui.notifications, message, options);
          };
          items.longbow = await addCompendiumItemToActor(actor, "pf1.weapons-and-ammo", "Composite Longbow");
        });
        after(() => {
          ui.notifications.error = originalError;
        });

        it("should have added an item", function () {
          expect(actor.items.getName("Composite Longbow")).to.be.ok;
        });

        it("should not be able to attack without arrows", async function () {
          const usage = await items.longbow.use({ skipDialog: true });
          expect(usage.code).to.equal(pf1.actionUse.ERR_REQUIREMENT.INSUFFICIENT_AMMO, "out of ammo error");
          expect(errorNotifications.pop()).to.be.equal(game.i18n.localize("PF1.AmmoDepleted"));
        });

        describe("attack without ammo usage", function () {
          let msg;
          before(async () => {
            await items.longbow.defaultAction.update({ ammo: { type: "none" } });
            const usage = await items.longbow.use({ skipDialog: true });
            msg = usage.shared.message;
            messages.push(msg);
          });

          it("should be a ChatMessage", function () {
            expect(msg).to.be.instanceOf(pf1.documents.ChatMessagePF);
          });
          it("should have the correct attack formula", function () {
            const parts = msg.systemRolls.attacks[0].attack.formula.split(" + ");
            expect(parts.shift()).to.equal("1d20");
            const expected = new Set(["2[Dexterity]"]);
            expect(new Set(parts)).to.deep.equal(expected);
          });
          it("should have the correct damage formula", function () {
            const parts = msg.systemRolls.attacks[0].damage[0].formula.split(" + ");
            const expected = new Set(["0[Strength]", "sizeRoll(1, 8, 4)"]);
            expect(new Set(parts)).to.deep.equal(expected);
          });
        });

        describe("attack with ammo usage and ammo present", function () {
          let usage;
          before(async () => {
            await items.longbow.defaultAction.update({ ammo: { type: "arrow" } });

            items.arrows = await addCompendiumItemToActor(actor, "pf1.weapons-and-ammo", "Arrow");
            await items.longbow.update({ "system.ammo.default": items.arrows.id });
            await items.arrows.update({ "system.abundant": false });
            usage = await items.longbow.use({ skipDialog: true, chatMessage: false });
          });

          it("should be an object", function () {
            expect(usage).to.be.an("object");
          });
          it("should have the correct attack formula", function () {
            const parts = usage.shared.chatData.system.rolls.attacks[0].attack.formula.split(" + ");
            expect(parts.shift()).to.equal("1d20");
            const expected = new Set(["2[Dexterity]"]);
            expect(new Set(parts)).to.deep.equal(expected);
          });
          it("should have the correct damage formula", function () {
            const parts = usage.shared.chatData.system.rolls.attacks[0].damage[0].formula.split(" + ");
            const expected = new Set(["min(4,0)[Strength]", "sizeRoll(1, 8, 4)"]);
            expect(new Set(parts)).to.deep.equal(expected);
          });
          it("should use ammo", function () {
            expect(items.arrows.system.quantity).to.equal(19);
          });
        });

        describe("attack with higher BAB and ammo present", function () {
          let msg;
          before(async () => {
            items.fighterClass = await addCompendiumItemToActor(actor, "pf1.classes", "Fighter", {
              system: { level: 10 },
            });
            const usage = await items.longbow.use({ skipDialog: true });
            msg = usage.shared.message;
            messages.push(msg);
          });
          after(async () => {
            await items.fighterClass.delete();
          });

          it("should be a ChatMessage", function () {
            expect(msg).to.be.instanceOf(pf1.documents.ChatMessagePF);
          });
          it("should have the correct attack formula", function () {
            const parts = msg.systemRolls.attacks[0].attack.formula.split(" + ");
            expect(parts.shift()).to.equal("1d20");
            expect(parts).to.have.lengthOf(2);
            const expected = new Set(["10[Base Attack Bonus]", "2[Dexterity]"]);
            expect(new Set(parts)).to.deep.equal(expected);
            // TODO: Add test for iterative attacks
            // TODO: Add test for additional ammo usage
          });
        });
      });
    },
    { displayName: "PF1: Actor – Attack Items" }
  );
};
