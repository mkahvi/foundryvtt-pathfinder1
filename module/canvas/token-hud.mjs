import { clearHighlight, showAttackReach } from "./attack-reach.mjs";
import { getSkipActionPrompt } from "module/documents/settings.mjs";
import { renderCachedTemplate } from "@utils/handlebars/templates.mjs";

/**
 * Extension of core Token HUD
 *
 * @since PF1 v11
 */
export class TokenHUDPF extends TokenHUD {
  _getStatusEffectChoices() {
    const core = super._getStatusEffectChoices(),
      buffs = {};

    const items = this.object.actor?.itemTypes.buff ?? [];
    for (const buff of items) {
      const id = `buff-${buff.id}`;
      buffs[id] = {
        _id: id, // to match v12
        id,
        title: buff.name,
        src: buff.img,
        isActive: buff.isActive,
        isOverlay: false,
        cssClass: buff.isActive ? "active" : "",
      };
    }

    return { ...core, ...buffs };
  }

  // V12 and prior
  activateListeners(html) {
    super.activateListeners(html);

    this.addQuickActions(this, html[0]);
  }

  // V13
  _onRender(context, options) {
    this.addQuickActions(this, this.element);
  }

  /**
   * Add quick action buttons to token HUD.
   *
   * @param {TokenHUD} app - HUD app
   * @param {Element} html - HUD element
   */
  async addQuickActions(app, html) {
    const token = app.object;
    const actor = token.actor;

    const quickActions = actor?.getQuickActions?.();

    if (!quickActions?.length) return;

    const templateData = {
      actions: quickActions,
    };

    const template = document.createElement("template");
    template.innerHTML = renderCachedTemplate("systems/pf1/templates/hud/quick-actions.hbs", templateData);
    const fragment = template.content;

    this.addQuickActionListeners(fragment);

    Hooks.callAll("pf1RenderQuickActions", app, token, fragment);

    html.querySelector(".col.middle").after(fragment);
  }

  /**
   * Add listeners to token HUD quick action element.
   *
   * @param {DocumentFragment} html - Quick actions element
   */
  addQuickActionListeners(html) {
    const showReach = game.settings.get("pf1", "performance").reachLimit >= 10;

    const token = this.object;
    const actor = token.actor;

    for (const el of html.querySelectorAll(".token-quick-action")) {
      el.addEventListener("click", (event) => {
        event.preventDefault();
        const itemId = event.target.dataset.itemId;
        const item = actor.items.get(itemId);
        if (!event.ctrlKey) {
          item.use({ ev: event, token: token.document, skipDialog: getSkipActionPrompt() });
        } else {
          item.displayCard({ token: token.document });
        }
      });

      el.addEventListener("contextmenu", (event) => {
        event.preventDefault();
        const itemId = event.target.dataset.itemId;
        const item = actor.items.get(itemId);
        item.sheet.render(true, { focus: true });
      });

      // Reach highlight on mouse hover
      if (showReach) {
        const itemId = el.dataset.itemId;
        const item = actor.items.get(itemId);
        const action = item.defaultAction;
        el.addEventListener("pointerenter", () => showAttackReach(token, action), { passive: true });
        el.addEventListener("pointerleave", () => clearHighlight(), { passive: true });
      }
    }
  }
}
