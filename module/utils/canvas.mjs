/**
 * Measure the distance between two pixel coordinates
 * See BaseGrid.measureDistance for more details
 *
 * @deprecated
 * @param segments
 * @param options
 */
export function measureDistances(segments, options = {}) {
  foundry.utils.logCompatibilityWarning(
    "pf1.utils.canvas.measureDistances() is deprecated in favor of canvas.grid.measureDistances() and canvas.grid.measurePath()",
    {
      since: "PF1 v11",
      until: "PF1 v12",
    }
  );
  return canvas.grid.measureDistances(segments, options);
}
