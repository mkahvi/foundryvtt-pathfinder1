export {};

declare module "./script-call.mjs" {
  interface ItemScriptCall {
    /**
     * Internal ID.
     */
    _id?: string;
    /**
     * Script name. If type is macro, this is overwritten by the macro name.
     */
    name?: string;
    /**
     * Script image. If type is macro, this is overwritten by the macro image.
     */
    img?: string;
    type: "script" | "macro";
    /**
     * If type is script, this is the script body.
     * If type is macro, this is the UUID to the macro.
     */
    value: string;
    category: string;
    /**
     * Hidden from normal user, even document owner.
     */
    hidden?: boolean;
  }
}
