import { ItemPF } from "./item-pf.mjs";

/**
 * Race item
 */
export class ItemRacePF extends ItemPF {
  /**
   * @override
   */
  static system = Object.freeze({
    ...super.system,
    hasIdentifier: false,
    hasActions: false,
  });

  /**
   * @internal
   * @override
   * @param {object} data - Creation data
   * @param {object} context - Context
   * @param {User} user - Triggering user
   */
  async _preCreate(data, context, user) {
    await super._preCreate(data, context, user);

    const actor = this.actor;
    if (!actor) return;

    // Overwrite race
    const oldRace = actor.itemTypes.race.find((r) => r !== this);
    if (oldRace) {
      const oldSize = oldRace.system.size;
      await oldRace.delete();

      const context = {};
      // Ensure actor size is updated to match the race, but only if it's same as old race
      const actorSize = actor.system.traits.size;
      if (actorSize !== this.system.size && oldSize === actorSize) context._pf1SizeChanged = true;
    }
  }

  /**
   * @internal
   * @override
   * @param {object} changed - Changed data
   * @param {object} context - Context
   * @param {User} user - Triggering user
   */
  async _preUpdate(changed, context, user) {
    await super._preUpdate(changed, context, user);
    if (!changed.system) return;
    if (context.diff === false || context.recursive === false) return; // Don't diff if we were told not to diff

    const actor = this.actor;

    // Track size change
    const newSize = changed.system?.size;
    if (actor && newSize !== undefined) {
      const oldSize = actor.system.traits?.size?.base;
      if (this.system.size === oldSize && newSize !== oldSize) {
        context._pf1SizeChanged = true;
      }
    }
  }

  prepareBaseData() {
    super.prepareBaseData();

    // Add humanoid if the race has nothing
    if (this.system.creatureTypes?.total?.size === 0) {
      this.system.creatureTypes.standard.add("humanoid");
    }
  }

  /**
   * @remarks This item type can not be recharged.
   * @override
   */
  recharge() {
    return;
  }
}
