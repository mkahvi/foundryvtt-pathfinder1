export {};

declare module "./materials.mjs" {
  interface Material extends RegistryEntry {
    /**
     * Simpler name when such is known.
     *
     * For example, "alchemical silver" is more commonly referred to simply as "silver"
     */
    shortName?: string;
    /**
     * Behaves for DR/ER exactly the same as another material.
     *
     * For example, mithral counts as silver in all regards, there is nothing that has DR penetrated by mithral specifically.
     */
    treatedAs: string;
    /**
     * Is add-on material
     */
    addon: boolean;
    /**
     * Is always available as base material
     *
     * Such as wood.
     */
    intrinsic: boolean;
    /**
     * Is primitive material
     *
     * For example: bone, wood, or stone.
     */
    primitive: boolean;
    /**
     * Materials this can be added on top of
     */
    baseMaterial: string[];
    hardness: number;
    hardnessMultiplier: number;
    healthPerInch: number;
    healthBonus: number;
    healthMultiplier: number;
    /**
     * Is this material always masterworked?
     *
     * @defaultValue `false`
     */
    masterwork: boolean;
    /**
     * What type of gear is this material allowed on?
     */
    allowed: {
      lightBlade: boolean;
      oneHandBlade: boolean;
      twoHandBlade: boolean;
      rangedWeapon: boolean;
      buckler: boolean;
      lightShield: boolean;
      heavyShield: boolean;
      towerShield: boolean;
      lightArmor: boolean;
      mediumArmor: boolean;
      heavyArmor: boolean;
    };
    /**
     * Armor modifiers
     */
    armor: {
      acp: number;
      maxDex: number;
      asf: number;
    };
    /**
     * Is this valid for DR choices?
     */
    dr: boolean;
    /**
     * Explicit incompatible materials when not otherwise determinable
     */
    incompatible: string[];
    /**
     * Price modifiers
     */
    price: {
      multiplier: number;
      perPound: number;
      ammunition: number;
      lightWeapon: number;
      oneHandWeapon: number;
      twoHandWeapon: number;
      rangedOneHandWeapon: number;
      rangedTwoHandWeapon: number;
      shield: number;
      lightArmor: number;
      mediumArmor: number;
      heavyArmor: number;
      enhancement: {
        /**
         * Bonus cost to apply enhancement. One time price increase on first enhancement.
         *
         * For example, used by cold iron
         */
        weapon: number;
      };
    };
    /**
     * Shield modifiers
     */
    shield: {
      acp: number;
      maxDex: number;
      asf: number;
    };
    /**
     * Weight modifiers
     */
    weight: {
      multiplier: number;
      bonusPerPound: number;
    };
  }
}
