_id: u9sTYTt9G8qyWbAg
_key: '!items!u9sTYTt9G8qyWbAg'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/violet_07.jpg
name: Ravener
system:
  changes:
    - _id: kem72dqu
      formula: max(1, floor(@abilities.cha.mod / 2))
      target: ac
      type: deflection
    - _id: urb48k3j
      formula: '4'
      target: str
      type: untyped
    - _id: u756fjkn
      formula: '4'
      target: int
      type: untyped
    - _id: vzhv5trc
      formula: '4'
      target: wis
      type: untyped
    - _id: pxtfhxte
      formula: '6'
      target: cha
      type: untyped
    - _id: 6hpqpwg4
      formula: '8'
      target: skill.int
      type: racial
    - _id: nl0cgkra
      formula: '8'
      target: skill.per
      type: racial
    - _id: wl2oeh93
      formula: '8'
      target: skill.ste
      type: racial
  crOffset: '2'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>Most evil dragons spend their
      lifetimes coveting and amassing wealth, but when the end draws near, some
      come to realize that all the wealth in the world cannot forestall death.
      Faced with this truth, most dragons vent their frustration on the
      countryside, ravaging the world before their passing. Yet some seek a
      greater solution to the problem and decide instead to linger on, hoarding
      life as they once hoarded gold. These foul wyrms attract the attention of
      dark powers, and through the blackest of necromantic rituals are
      transformed into undead dragons known as raveners.<p>Although its body
      quickly rots away, a ravener does not care for the needs of the flesh. It
      seeks only to consume life, be it from wild animals, would-be
      dragonslayers, or even other dragons. A ravener is often on the move,
      changing lairs frequently as its territories become devoid of life.<p>The
      ravener presented here is built from a red dragon wyrm.<p>"Ravener" is an
      acquired template that can be added to any evil true dragon of an age
      category of ancient or older (referred to hereafter as the base creature).
      A ravener retains all the base creature’s statistics and special abilities
      except as noted here.<p><b>CR:</b> Same as the base creature +2.</p>

      <p><b>Alignment:</b> Any evil.</p>

      <p><b>Type:</b> The creature’s type changes to undead. Do not recalculate
      BAB, saves, or skill ranks. It keeps any subtypes possessed by the base
      creature.</p>

      <p><b>Senses:</b> A ravener’s darkvision increases to 240 feet, and its
      blindsense increases to 120 feet.</p>

      <p><b>Armor Class:</b> A ravener gains a deflection bonus to its AC equal
      to half its Charisma bonus (minimum +1).</p>

      <p><b>Hit Dice:</b> Change all of the base creature’s racial Hit Dice to
      d8s. All Hit Dice derived from class levels remain unchanged. As an
      undead, a ravener uses its Charisma to determine bonus hit points instead
      of its Constitution.</p>

      <p><b>Saving Throws:</b> As undead, a ravener uses its Charisma modifier
      on Fortitude saves (instead of Constitution).</p>

      <p><b>Defensive Abilities:</b> A ravener gains channel resistance +4 and
      all of the immunities derived from undead traits. Its damage reduction
      changes from DR/magic to DR/good. A ravener also gains the following
      ability.</p>

      <p><em>Soul Ward (Su): </em>An intangible field of siphoned soul energy
      protects a ravener from destruction. This ward has a maximum number of hit
      points equal to twice the ravener’s Hit Dice, but starts at half this
      amount. Whenever a ravener would be reduced below 1 hit point, all damage
      in excess of that which would reduce it to 1 hit point is instead dealt to
      its soul ward. If this damage reduces the soul ward to fewer than 0 hit
      points, the ravener is destroyed.</p>

      <p><b>Attacks:</b> A ravener retains all of the natural attacks of the
      base creature, but each of these attacks threatens a critical hit on a 19
      or 20. Feats like Improved Critical can increase this range further. If
      the ravener scores a critical hit with a natural weapon, the target gains
      1 negative level. The DC to remove this negative level is equal to 10 +
      1/2 the ravener’s Hit Dice + the ravener’s Charisma modifier. Whenever a
      creature gains a negative level in this way, the ravener adds 5 points to
      its soul ward.</p>

      <p><b>Special Attacks:</b> A ravener retains all of the special attacks of
      the base creature and gains the following special attacks as described
      below. All save DCs are equal to 10 + 1/2 the ravener’s HD + the ravener’s
      Charisma modifier.</p>

      <p><em>Breath Weapon (Su): </em>A ravener keeps the breath weapon of the
      base creature—the save DC for this breath weapon is now Charisma-based. In
      addition, a ravener’s breath weapon bestows 2 negative levels on all
      creatures in the area. A successful Reflex save halves the damage and
      reduces the energy drain to 1 negative level. The save DC to remove these
      negative levels is equal to the ravener’s breath weapon DC. The ravener
      adds 1 hit point to its soul ward ability for each negative level bestowed
      in this way.</p>

      <p><em>Cowering Fear (Su): </em>Any creature shaken by the ravener’s
      frightful presence is cowering instead of shaken for the first round of
      the effect, and shaken for the rest of the duration. Any creature that is
      panicked by its frightful presence is instead cowering for the
      duration.</p>

      <p><em>Soul Consumption (Su): </em>When a living creature within 30 feet
      of a ravener dies, that creature’s soul is torn from its body and pulled
      into the ravener’s maw if the dying creature fails a Will save (DC equals
      the save DC of the ravener’s breath weapon). This adds a number of hit
      points to the ravener’s soul ward equal to the dead creature’s Hit Dice.
      Creatures that have their souls consumed in this way can only be brought
      back to life through miracle, true resurrection, or wish.</p>

      <p><em>Soul Magic (Sp): </em>A ravener retains the base creature’s
      spellcasting capability, adding three levels to the base creature’s caster
      level. This increases the number of spells known by the ravener, but the
      ravener loses all spell slots. Instead, whenever the ravener wishes to
      cast any one of its spells known, it consumes a number of hit points from
      its soul ward equal to the spell slot level necessary to cast the spell
      (including increased levels for metamagic feats and so on). If the soul
      ward has insufficient hit points, the ravener cannot cast that spell.
      Casting a spell that reduces its soul ward to exactly 0 hit points does
      not harm the ravener (though most are not comfortable without this buffer
      of soul-energy and try to replenish it quickly).</p>

      <p><b>Abilities:</b> Str +4, Int +4, Wis +4, Cha +6. Being undead, a
      ravener has no Constitution score.</p>

      <p><b>Skills:</b> A ravener has a +8 racial bonus on Intimidate,
      Perception, and Stealth checks. The ravener’s class skills are otherwise
      the same as those of the base creature.</p>
  subType: template
type: feat
